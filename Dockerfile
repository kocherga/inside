FROM python:3.7.2

RUN apt-get update && apt-get install -y snmp

RUN pip install --upgrade pip && pip install pip-tools

WORKDIR /code
COPY requirements.txt /code/
RUN pip-sync

COPY . /code/
