#!/usr/bin/env python3

import time
import subprocess
import re

from prometheus_client import start_http_server, REGISTRY
from prometheus_client.core import GaugeMetricFamily

def parse_oid(oid_string):
    return oid_string.split('.')

def parse_line(line):
    match = re.match(r'(\S+)\s=\s(STRING|INTEGER):\s+(.*)$', line)
    if not match:
        raise Exception(f"Can't parse {line}")

    oid = parse_oid(match.group(1))
    value_type = match.group(2)
    if value_type == 'STRING':
        value = re.match(r'"(.*)"$', match.group(3)).group(1).strip()
    elif value_type == 'INTEGER':
        value = int(match.group(3))
    else:
        raise Exception(f"Unknown type {value_type}")

    return {
        "oid": oid,
        "type": value_type,
        "value": value,
    }

def walk(ip, root_oid_string):
    result = subprocess.check_output(
        ['snmpwalk', '-v2c', '-c', 'public', ip, root_oid_string],
        encoding='utf-8',
    )
    lines = result.split('\n')
    for line in lines:
        if not line.strip():
            continue
        yield parse_line(line)

class PrinterCollector:
    def collect(self):
        supplies_max_level = GaugeMetricFamily(
            'printer_supplies_max_level',
            'Max level of printer supplies',
            labels=['color']
        )
        supplies_current_level = GaugeMetricFamily(
            'printer_supplies_current_level',
            'Current level of printer supplies',
            labels=['color']
        )

        ip = '192.168.88.29'
        id2color = {}
        for parsed in walk(ip, 'iso.3.6.1.2.1.43.11.1.1.6'):
            if 'Toner Cartridge' not in parsed['value']:
                continue
            id2color[parsed['oid'][-1]] = parsed['value'].split(' ')[0]

        for parsed in walk(ip, 'iso.3.6.1.2.1.43.11.1.1.8'):
            last_id = parsed['oid'][-1]
            if last_id not in id2color:
                continue

            supplies_max_level.add_metric([id2color[last_id]], parsed['value'])

        yield supplies_max_level

        for parsed in walk(ip, 'iso.3.6.1.2.1.43.11.1.1.9'):
            last_id = parsed['oid'][-1]
            if last_id not in id2color:
                continue
            supplies_current_level.add_metric([id2color[last_id]], parsed['value'])

        yield supplies_current_level


start_http_server(80)
REGISTRY.register(PrinterCollector())

while True:
    time.sleep(15)
