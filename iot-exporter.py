#!/usr/bin/env python3

import re

from prometheus_client import start_http_server, Summary, Gauge
import paho.mqtt.subscribe as subscribe

metrics = {}
temp = Gauge('sensor_temperature', 'Temperature sensor data', ['room'])
humidity = Gauge('sensor_humidity', 'Humidity sensor data', ['room'])

def extract_to_gauge(data, key, gauge):
    match = re.search("'" + key + r"': (\d+(?:\.\d+)?)", data)
    if match:
        value = match.group(1)
        print(f"{key}: {value}")
        gauge.labels(room='geb').set(value)

def callback_function(client, userdata, message):
    topic = message.topic
    payload = message.payload.decode("utf-8")

    extract_to_gauge(payload, 'temp', temp)
    extract_to_gauge(payload, 'humidity', humidity)

start_http_server(80)

subscribe.callback(callback_function, "kch_sensors/datastream", hostname='192.168.88.145')
